package org.stmare.assist;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.ui.text.java.JavaContentAssistInvocationContext;

public class AstUtil {
    public static ASTNode createAst(JavaContentAssistInvocationContext javaContext) {
        ASTParser parser = ASTParser.newParser(AST.JLS4);
        parser.setResolveBindings(true);
        parser.setSource(javaContext.getCompilationUnit());
        parser.setStatementsRecovery(true);
        ASTNode ast = parser.createAST(null);
        return ast;
    }
    
    public static ASTNode calculateCoveringASTNode(JavaContentAssistInvocationContext javaContext) {
        ASTParser parser = ASTParser.newParser(AST.JLS4);
        parser.setResolveBindings(true);
        parser.setSource(javaContext.getCompilationUnit());
        int position = javaContext.getInvocationOffset();
        parser.setStatementsRecovery(true);
        ASTNode ast = parser.createAST(null);
        
        ASTNode coveringNode = null;
        ASTNode lower = ast;
        while(lower != null) {
            coveringNode = lower;
            lower = null;
            @SuppressWarnings("unchecked")
            List<StructuralPropertyDescriptor> properties = coveringNode.structuralPropertiesForType();
            for(StructuralPropertyDescriptor descriptor : properties) {
                if(descriptor.isChildProperty()) {
                    ASTNode child = (ASTNode) coveringNode.getStructuralProperty(descriptor);
                    if(child != null && AstUtil.covers(child, position)) {
                        lower = child;
                    }
                } else if(descriptor.isChildListProperty()) {
                    @SuppressWarnings("unchecked")
                    List<? extends ASTNode> children = (List<? extends ASTNode>) coveringNode.getStructuralProperty(descriptor);
                    for(ASTNode child : children) {
                        if(AstUtil.covers(child, position)) {
                            lower = child;
                            //calculateCoveringASTNode((ICompilationUnit) lower);
                            break;
                        }
                    }
                }
            }
        }
        return coveringNode;
    }

    public static boolean covers(ASTNode node, int position) {
        return node.getStartPosition() < position && node.getStartPosition() + node.getLength() > position;
    }

}
