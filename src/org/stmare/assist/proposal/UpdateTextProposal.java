package org.stmare.assist.proposal;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.stmare.assist.Activator;

public class UpdateTextProposal implements ICompletionProposal {
    /**
     * Text to be inserted
     */
    private String text;
    
    /**
     * Offset in text where update starts 
     */
    private int offset;

    /**
     * Length to be replaced
     * 0 means insertion
     */
    private int length;

    /**
     * Display in list of proposals
     */
    private String displayString;
    
    /**
     * Set position after action
     * 0 = at the end
     * negative before changed text, positive behind text
     */
    private int selectionOffsetRelatively;

    private Image image;

    private String additionalProposalInfo;
    
    public UpdateTextProposal(String text, int offset, int length) {
        super();
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = text;
    }
    
    public UpdateTextProposal(String text, int offset, int length, String displayString, int selectionOffsetRelatively) {
        super();
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = displayString;
        this.selectionOffsetRelatively = selectionOffsetRelatively;
    }

    public UpdateTextProposal(String text, int offset, int length, String displayString, int selectionOffsetRelatively, Image image, String additionalProposalInfo) {
        super();
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = displayString;
        this.selectionOffsetRelatively = selectionOffsetRelatively;
        this.image = image;
        this.additionalProposalInfo = additionalProposalInfo;
    }
    
    @Override
    public void apply(IDocument document) {
        try {
            document.replace(offset, length, text);
        } catch (BadLocationException e) {
            Activator.logError(e);
        }        
    }

    @Override
    public String getAdditionalProposalInfo() {
        return additionalProposalInfo;
    }

    @Override
    public IContextInformation getContextInformation() {
        return null;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public Point getSelection(IDocument document) {
        return new Point(this.offset + this.text.length() + selectionOffsetRelatively, 0);
    }
}
