package org.stmare.assist.proposal;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IPackageDeclaration;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.text.edits.TextEdit;
import org.stmare.assist.Activator;

public class AddClassProposal implements ICompletionProposal {
    /**
     * Text to be inserted
     */
    private String text;
    
    /**
     * Offset in text where update starts 
     */
    private int offset;

    /**
     * Length to be replaced
     * 0 means insertion
     */
    private int length;

    /**
     * Display in list of proposals
     */
    private String displayString;
    
    /**
     * Set position after action
     * 0 = at the end
     * negative before changed text, positive behind text
     */
    private int selectionOffsetRelatively;

    private Image image;

    private String additionalProposalInfo;
    
    private int importChangeDelta;

    private ICompilationUnit iCompilationUnit;

    private String insertClass;

    private String fullyQualifiedName;

    private String importQualName;
    
    public AddClassProposal(ICompilationUnit iCompilationUnit, String text, int offset, int length) {
        super();
        this.iCompilationUnit = iCompilationUnit;
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = text;
        init();
    }
    
    public AddClassProposal(ICompilationUnit iCompilationUnit, String text, int offset, int length, String displayString, int selectionOffsetRelatively) {
        super();
        this.iCompilationUnit = iCompilationUnit;
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = displayString;
        this.selectionOffsetRelatively = selectionOffsetRelatively;
        init();
    }

    public AddClassProposal(ICompilationUnit iCompilationUnit, String text, int offset, int length, String displayString, int selectionOffsetRelatively, Image image, String additionalProposalInfo) {
        super();
        this.iCompilationUnit = iCompilationUnit;
        this.text = text;
        this.offset = offset;
        this.length = length;
        this.displayString = displayString;
        this.selectionOffsetRelatively = selectionOffsetRelatively;
        this.image = image;
        this.additionalProposalInfo = additionalProposalInfo;
        init();
    }
    
    private void init() {
        fullyQualifiedName = text;
        int indexOf = Math.min(text.indexOf("<"), text.indexOf("("));
        if (indexOf != -1) {
            fullyQualifiedName = fullyQualifiedName.substring(0, indexOf);
        }
    }
    
    @Override
    public void apply(IDocument document) {
        try {
            insertClass = text;
            if (importQualName != null) {
                ImportResult addImport = addImport(importQualName);

                if (text.indexOf(".") != -1) {
                    // neni to trida z root package
                    importChangeDelta = addImport.changeDelta;
                    if (importChangeDelta != 0 || addImport.exists || addImport.currentPackage) {
                        int lastIndexOf = text.lastIndexOf(".");
                        if (lastIndexOf > 0) {
                            insertClass = text.substring(lastIndexOf + 1);
                        }
                    }
                }
            }
            
            document.replace(offset + importChangeDelta, length, insertClass);
        } catch (BadLocationException e) {
            Activator.logError(e);
        }        
    }

    @Override
    public String getAdditionalProposalInfo() {
        return additionalProposalInfo;
    }

    @Override
    public IContextInformation getContextInformation() {
        return null;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public Point getSelection(IDocument document) {
        return new Point(offset + insertClass.length() + importChangeDelta + selectionOffsetRelatively, 0);
    }

    public ImportResult addImport(String addImport) {
        int lastIndexOf = addImport.lastIndexOf(".");
        ImportResult importResult = new ImportResult();
        try {
            IPackageDeclaration[] packageDeclarations = iCompilationUnit.getPackageDeclarations();
            if (packageDeclarations.length > 0) {
                IPackageDeclaration iPackageDeclaration = packageDeclarations[0];
                String elementName = iPackageDeclaration.getElementName();
                if (lastIndexOf > 0) {
                    String packageName = addImport.substring(0, lastIndexOf);
                    if (packageName.equals(elementName)) {
                        importResult.currentPackage = true;
                        return importResult;
                    }
                }
            }
        } catch (JavaModelException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            String sourceOrig = iCompilationUnit.getSource();
            ImportRewrite irw = ImportRewrite.create(iCompilationUnit, true);
            irw.addImport(addImport);
            TextEdit edit = irw.rewriteImports(null);
            iCompilationUnit.applyTextEdit(edit, null);
            IImportDeclaration[] imports = iCompilationUnit.getImports();
            if (lastIndexOf > 0) {
                String packageName = addImport.substring(0, lastIndexOf);
                String pacPlusAst = packageName + ".*";
                for (IImportDeclaration iImportDeclaration : imports) {
                    String elementName = iImportDeclaration.getElementName();
                    if (elementName.equals(importQualName) || elementName.equals(pacPlusAst)) {
                        importResult.exists = true;
                        break;
                    }
                }
            }
            String sourceNew = iCompilationUnit.getSource();
            importResult.changeDelta = sourceNew.length() - sourceOrig.length();
        } catch (Exception e) {
            // no handling
        }
        return importResult;
    }
    
    public static class ImportResult {
        /**
         * True pokud je trida v seznamu importu obsazena (nezavisle na tom, jestli byla prave pridana)
         */
        private boolean exists;
        
        private int changeDelta;
        
        /**
         * Pridani importu neni potreba - jedna se o tridu ve stejne package
         */
        private boolean currentPackage;
    }

    public void setImportQualName(String importQualName) {
        this.importQualName = importQualName;
    }
}
