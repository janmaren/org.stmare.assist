package org.stmare.assist;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext;
import org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer;
import org.eclipse.jdt.ui.text.java.JavaContentAssistInvocationContext;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.custom.StyledText;
import org.stmare.assist.proposal.UpdateTextProposal;

/**
 * Offers variable names to be used as arguments for method calls.
 * Variable name is derived from appropriate type or from name of setter 
 * @author Standa
 */
public class MethodParametersOffer implements IJavaCompletionProposalComputer {
    private final static Pattern SETTER_PATTERN = Pattern.compile("\\w+\\s*\\.\\s*set([\\w\\$]+)\\s*\\(\\s*");
    

    @Override
    public List<ICompletionProposal> computeCompletionProposals(ContentAssistInvocationContext context, IProgressMonitor arg1) {
        JavaContentAssistInvocationContext jcontext = null;
        if (context instanceof JavaContentAssistInvocationContext) {
            jcontext = (JavaContentAssistInvocationContext) context;
        } else {
            return new ArrayList<ICompletionProposal>();
        }
        int offset = jcontext.getInvocationOffset();

        final StyledText textWidget = jcontext.getViewer().getTextWidget();
                
        HashSet<String> suggestedNames = new HashSet<String>();

        // way 1: by name of setter
        doArgumentBySetter(suggestedNames, jcontext.getDocument(), offset, textWidget);
        
        // way 2: by type of agument
        doArgumentByType(suggestedNames, jcontext.getDocument(), jcontext);
        
        final ArrayList<ICompletionProposal> proposalList = new ArrayList<ICompletionProposal>();
        for (String value : suggestedNames) {
            proposalList.add(new UpdateTextProposal(value, offset, textWidget.getSelectionCount() > 0 ? textWidget.getSelectionCount(): 0, value, 0));
        }
        
        return proposalList;
    }

    private void doArgumentByType(HashSet<String> suggestedNames, IDocument document, JavaContentAssistInvocationContext jcontext) {
        ASTNode coveringNode = AstUtil.calculateCoveringASTNode(jcontext);
        
        if (coveringNode instanceof org.eclipse.jdt.core.dom.MethodInvocation || coveringNode instanceof org.eclipse.jdt.core.dom.ClassInstanceCreation) {
            IMethodBinding resolveTypeBinding = null;
            org.eclipse.jdt.core.dom.MethodInvocation methodInvocation = null;
            if (coveringNode instanceof org.eclipse.jdt.core.dom.MethodInvocation) {
                methodInvocation = (org.eclipse.jdt.core.dom.MethodInvocation) coveringNode;
                resolveTypeBinding = methodInvocation.resolveMethodBinding();
            } else if (coveringNode instanceof org.eclipse.jdt.core.dom.ClassInstanceCreation) {
                org.eclipse.jdt.core.dom.ClassInstanceCreation classInstanceCreation = (ClassInstanceCreation) coveringNode;
                resolveTypeBinding = classInstanceCreation.resolveConstructorBinding();
            } else {
                throw new RuntimeException("Unknown type");
            }
            if (methodInvocation != null) {
                SimpleName possibleSetterName = methodInvocation.getName();
                String fullyQualifiedName = possibleSetterName.getFullyQualifiedName();
                if (fullyQualifiedName.startsWith("set") && fullyQualifiedName.length() > 3 && Character.isUpperCase(fullyQualifiedName.charAt(3))) {
                    String varSetName = null;
                    if (fullyQualifiedName.length() == 4) {
                        varSetName = fullyQualifiedName.substring(3).toLowerCase();
                    } else {
                        varSetName = fullyQualifiedName.substring(3, 4).toLowerCase() + fullyQualifiedName.substring(4);
                    }
                    //suggestedNames.add(varSetName); // normal variant
                }
            }            
            if (resolveTypeBinding != null && resolveTypeBinding.getParameterTypes().length > 0) {
                int paramIndex = getColonNumberBeforeUntilOpen(document.get(), jcontext.getInvocationOffset());
                if (resolveTypeBinding.getParameterTypes().length > paramIndex) {
                    ITypeBinding iTypeBinding = resolveTypeBinding.getParameterTypes()[paramIndex];
                    String name = iTypeBinding.getName();
                    int length = name.length();
                    boolean isArray = false;
                    if (name.endsWith("[]")) {
                        length -= 2;
                        isArray = true;
                    } else if (name.endsWith(">")) {
                        length = name.indexOf("<");
                        if (length == -1) {
                            return;
                        }
                     }
    
                    String varTypeName = name.substring(0, 1).toLowerCase() + name.substring(1, length);
                    if ("class".equals(varTypeName)) {
                        varTypeName = "clazz";
                    } else if (!isArray && ("int".equals(varTypeName) || "long".equals(varTypeName) || "short".equals(varTypeName) || "boolean".equals(varTypeName) || "float".equals(varTypeName) ||
                            "double".equals(varTypeName) || "char".equals(varTypeName) || "byte".equals(varTypeName))) {
                        varTypeName += "Var";
                    }
    
                    if (isArray) {
                        suggestedNames.add(varTypeName + "Array"); // ends with Array variant
                    } else {
                        suggestedNames.add(varTypeName); // normal variant
                    }
                }
            }
        }
    }
    
    /**
     * Number of colons before position
     * It can be used to get index of current parameter (zero based)
     * @param str
     * @param pos
     * @return
     */
    private static int getColonNumberBeforeUntilOpen(String str, int pos) {
        int number = 0;
        while (pos > 0) {
            pos--;
            char charAt = str.charAt(pos);
            if (charAt == '(') {
                return number;
            }
            if (charAt == ',') {
                number ++;
            }
        }
        return number;
    }
    
	private void doArgumentBySetter(final HashSet<String> suggestedNames, IDocument iDocument, int offset, StyledText textWidget) {
        String string = null;
        try {
            string = iDocument.get(0, iDocument.getLength());
        } catch (BadLocationException e) {
            Activator.logError(e);
        }
        
        Matcher matcher = SETTER_PATTERN.matcher(string);
        while (matcher.find()) {
        	if (matcher.start() >= offset) {
        		continue;
        	}
            if ((matcher.start() < offset && matcher.end() + textWidget.getSelectionCount() >= offset) ) {
                char firstCharBeforeOffset = firstCharBeforeOffset(offset, string);
                if (firstCharBeforeOffset != '(') {
                    continue; // must be behind opening
                }
                String setterName = matcher.group(1);
                String firstInSetter = setterName.substring(0, 1);
                String firstInField = firstInSetter.toLowerCase();
                if (!firstInSetter.equals(firstInField)) {
                    // it musn't be 'setemployee' but 'setEmployee'
                    String value = firstInField + setterName.substring(1);
                    suggestedNames.add(value);
                }
            }
        }
    }

    private static char firstCharBeforeOffset(int offset, String text) {
        while (--offset > 1) {
            char charAt = text.charAt(offset);
            if (charAt == '\r' ||
                            charAt == '\n' ||
                            charAt == ' ' ||
                            charAt == '\t') {
                continue;
            }
            return charAt;
        }
        return '\0';
    }
    
    @Override
    public List<IContextInformation> computeContextInformation(ContentAssistInvocationContext arg0, IProgressMonitor arg1) {
        return new ArrayList<IContextInformation>();
    }

    @Override
    public String getErrorMessage() {
        return null;
    }

    @Override
    public void sessionEnded() {

    }

    @Override
    public void sessionStarted() {

    }
}
