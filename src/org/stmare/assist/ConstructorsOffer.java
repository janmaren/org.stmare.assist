package org.stmare.assist;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.TypeNameMatch;
import org.eclipse.jdt.core.search.TypeNameMatchRequestor;
import org.eclipse.jdt.internal.corext.util.TypeNameMatchCollector;
import org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext;
import org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer;
import org.eclipse.jdt.ui.text.java.JavaContentAssistInvocationContext;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.custom.StyledText;
import org.stmare.assist.proposal.AddClassProposal;

@SuppressWarnings("restriction")
public class ConstructorsOffer implements IJavaCompletionProposalComputer {
    private static final StrUtil.CharInterval[] CLASS_NAME_CHARS = {
        new StrUtil.CharInterval('A', 'Z'),
        new StrUtil.CharInterval('a', 'z'),
        new StrUtil.CharInterval('$', '$'),
        new StrUtil.CharInterval('_', '_'),
        new StrUtil.CharInterval('0', '9'),
    }; 
    
    @Override
    public List<ICompletionProposal> computeCompletionProposals(ContentAssistInvocationContext context, IProgressMonitor arg1) {
        JavaContentAssistInvocationContext jcontext = null;
        if (context instanceof JavaContentAssistInvocationContext) {
            jcontext = (JavaContentAssistInvocationContext) context;
        } else {
            return new ArrayList<ICompletionProposal>();
        }
        int invocationOffset = jcontext.getInvocationOffset();
        String text = jcontext.getDocument().get();
        int findNonIntervalPosBackward = StrUtil.findNonIntervalPosBackward(text, invocationOffset, CLASS_NAME_CHARS);
        if (findNonIntervalPosBackward == -1) {
            return new ArrayList<ICompletionProposal>();
        }
        boolean instanceClass = false;
        if (Character.isWhitespace(text.charAt(findNonIntervalPosBackward))) {
            int findNonWhiteSpacePosBackward = StrUtil.findNonWhiteSpacePosBackward(text, findNonIntervalPosBackward);
            if (findNonWhiteSpacePosBackward == -1) {
                return new ArrayList<ICompletionProposal>();
            }
            if (text.charAt(findNonWhiteSpacePosBackward) == '<' || text.charAt(findNonWhiteSpacePosBackward) == ',') {
                instanceClass = false;
            } else if (StrUtil.isSequenceBack(text, findNonWhiteSpacePosBackward, "new")) {
                instanceClass = true;
            }
        } else {
            if (text.charAt(findNonIntervalPosBackward) != '.') {
                instanceClass = false;
            } else {
                return new ArrayList<ICompletionProposal>();
            }
        }
        
        String enteredText = text.substring(findNonIntervalPosBackward + 1, invocationOffset);
        
        final StyledText textWidget = jcontext.getViewer().getTextWidget();
        int offset = findNonIntervalPosBackward + 1;
        int length = invocationOffset - (findNonIntervalPosBackward + 1) + textWidget.getSelectionCount();
        final ArrayList<ICompletionProposal> proposalList = new ArrayList<ICompletionProposal>();
        if (instanceClass) {
            Set<Offer> matchingInstanceTypes = getMatchingInstanceTypes(jcontext, enteredText);
            addInstanceProposalList(matchingInstanceTypes, offset, length, proposalList, jcontext);
        } else {
            Set<Offer> matchingLeftTypes = new HashSet<ConstructorsOffer.Offer>();
            matchingLeftTypes = getMatchingInstanceTypes(jcontext, enteredText);
            addClassProposalList(matchingLeftTypes, offset, length, proposalList, jcontext);
        }
        return proposalList;
    }

    private void addClassProposalList(Set<Offer> offers, int offset, int length, List<ICompletionProposal> proposalList, JavaContentAssistInvocationContext context) {
        ICompilationUnit iCompilationUnit = context.getCompilationUnit();
        for (Offer offer : offers) {
            if (offer.getSourceFileType() != null) {
                AddClassProposal updateTextProposal = new AddClassProposal(iCompilationUnit, offer.getSourceFileType(), offset, length, offer.getSourceFileType(), 0);
                proposalList.add(updateTextProposal);
            } else {
                String fullyQualifiedName = offer.getFullyQualifiedName();
                String displayText = offer.getSimpleTypeName();
                if (!"".equals(offer.getPackageName())) {
                    displayText += " - " + offer.getPackageName();
                }
                AddClassProposal updateTextProposal = new AddClassProposal(iCompilationUnit, fullyQualifiedName, offset, length, displayText, 0);
                updateTextProposal.setImportQualName(fullyQualifiedName);
                proposalList.add(updateTextProposal);
            }
        }
    }    

    private void addInstanceProposalList(Set<Offer> matchingTypes, int offset, int length, List<ICompletionProposal> proposalList, JavaContentAssistInvocationContext context) {
        for (Offer offer : matchingTypes) {
            int constructorArgs = offer.getConstructorArgs();

            String fullyQualifiedName = offer.getFullyQualifiedName();
            boolean genericArgumentsExists = offer.isGeneric();
            int moveOffset = 0;
            String insertText = fullyQualifiedName;
            String displayText = offer.getSimpleTypeName();
            if (genericArgumentsExists) {
                insertText += "<>()";
                displayText += "<>(" + (constructorArgs > 0 ? "..." : "") + ")";
                moveOffset = -3;
                if (constructorArgs == -1) { // instance z interface (anonymni trida)
                    insertText += "{}";
                    moveOffset -= 2; // melo by to vyjit do generic casti
                }
            } else {
                insertText += "()";
                displayText += "(" + (constructorArgs > 0 ? "..." : "") + ")";
                moveOffset = constructorArgs > 0 ? -1 : 0;
                if (constructorArgs == -1) { // instance z interface (anonymni trida)
                    insertText += "{}";
                    moveOffset -= 1; // (-2 + 1) melo by to vyjit do slozenych zavorek
                }
            }
            if (!"".equals(offer.getPackageName())) {
                displayText += " - " + offer.getPackageName();
            }
            
            ICompilationUnit iCompilationUnit = context.getCompilationUnit();
            AddClassProposal addClassProposal = new AddClassProposal(iCompilationUnit, insertText, offset, length, displayText, moveOffset);
            addClassProposal.setImportQualName(fullyQualifiedName);
            proposalList.add(addClassProposal);
        }
    }

    /**
     * -1 - no contructor
     * >= 0 - max number of arguments
     * @param typeNameMatch
     * @return
     */
    private static int constructorArguments(TypeNameMatch typeNameMatch) {
        int arguments = -1;
        try {
            IMethod[] methods = typeNameMatch.getType().getMethods();
            if (methods.length == 0) {
                return 0;
            }
            for (IMethod iMethod : methods) {
                if (iMethod.isConstructor()) {
                    if ((iMethod.getFlags() & Flags.AccPrivate) == Flags.AccPrivate) {
                        continue;
                    }
                    int parLength = iMethod.getParameters().length;
                    if (parLength > arguments) {
                        arguments = parLength;
                    }
                }
            }
        } catch (JavaModelException e1) {
        }
        return arguments;
    }

    private static int constructorArguments(ITypeBinding typeNameMatch) {
        int arguments = -1;
        IMethodBinding[] declaredMethods = typeNameMatch.getDeclaredMethods();
        if (declaredMethods.length == 0) {
            return 0;
        }
        for (IMethodBinding iMethod : declaredMethods) {
            if (iMethod.isConstructor()) {
                if ((iMethod.getModifiers() & Flags.AccPrivate) == Flags.AccPrivate) {
                    continue;
                }
                
                int parLength = iMethod.getParameterTypes().length;
                if (parLength > arguments) {
                    arguments = parLength;
                }
            }
        }
        return arguments;
    }
    
    @Override
    public List<IContextInformation> computeContextInformation(ContentAssistInvocationContext arg0, IProgressMonitor arg1) {
        return new ArrayList<IContextInformation>();
    }

    @Override
    public String getErrorMessage() {
        return null;
    }

    @Override
    public void sessionEnded() {

    }

    @Override
    public void sessionStarted() {

    }

    private List<TypeNameMatch> getMatchingTypes(IJavaProject javaProject, String prefix) {
        IJavaProject[] elements = new IJavaProject[] { javaProject };
        IJavaSearchScope scope = SearchEngine.createJavaSearchScope(elements);
        List<TypeNameMatch> result = new ArrayList<TypeNameMatch>();
        TypeNameMatchRequestor requestor = new TypeNameMatchCollector(result);
        SearchEngine engine = new SearchEngine();
        try {
            engine.searchAllTypeNames(null, 0, prefix.toCharArray(), SearchPattern.R_PREFIX_MATCH, IJavaSearchConstants.TYPE, scope, requestor,
                    IJavaSearchConstants.WAIT_UNTIL_READY_TO_SEARCH, null);
        } catch (JavaModelException e) {
            Activator.logError(e);
        }
        return result;
    }
    
    private Set<Offer> getMatchingInstanceTypes(JavaContentAssistInvocationContext jcontext, String enteredText) {
        Set<Offer> typeBindings = new LinkedHashSet<Offer>();
        
        ASTNode astNode = AstUtil.calculateCoveringASTNode(jcontext);
        if (astNode instanceof Block) {
            Block blockNode = (Block) astNode;
            ASTNode parent = blockNode.getParent();
            if (parent instanceof MethodDeclaration) {
                MethodDeclaration methodDeclaration = (MethodDeclaration) parent;
                IMethodBinding methodBinding = methodDeclaration.resolveBinding();
                if (methodBinding != null && !methodBinding.isConstructor()) {
                    addMethodParameterTypes(methodBinding, typeBindings, enteredText, methodDeclaration);
                    addMethodReturnType(methodBinding, typeBindings, enteredText, methodDeclaration);
                }
            }
        }
        
        if (enteredText.length() > 1) {
            addProjectTypes(jcontext.getProject(), typeBindings, enteredText);
        }
        
        return typeBindings;
    }

    private void addProjectTypes(IJavaProject project, Set<Offer> typeBindings, String enteredText) {
        List<TypeNameMatch> matchingTypes = getMatchingTypes(project, enteredText);
        for (TypeNameMatch typeNameMatch : matchingTypes) {
            IType type = typeNameMatch.getType();
            boolean generics = false;
            try {
                generics = type.getTypeParameters().length > 0; 
            } catch (JavaModelException e) {
            }
            boolean isPublic = false;
            try {
                if ((type.getFlags() & Flags.AccPublic) == Flags.AccPublic) {
                    isPublic = true;
                }
            } catch (JavaModelException e) {
            }
            if (isPublic) {
                int constructorArguments = constructorArguments(typeNameMatch);
                Offer offer = new Offer(typeNameMatch.getFullyQualifiedName(), typeNameMatch.getSimpleTypeName(), typeNameMatch.getPackageName(), generics, constructorArguments);
                typeBindings.add(offer);
            }
        }
        
    }

    private void addMethodParameterTypes(IMethodBinding methodBinding, Set<Offer> typeBindings, String enteredText, MethodDeclaration methodDeclaration) {
        ITypeBinding[] typeArguments = methodBinding.getParameterTypes();
        for (ITypeBinding iTypeBinding : typeArguments) {
            addTypeBinding(typeBindings, enteredText, iTypeBinding, null);
        }
    }

    private void addTypeBinding(Set<Offer> typeBindings, String enteredText, ITypeBinding iTypeBinding, String sourceFileType) {
        if (!iTypeBinding.isPrimitive() && iTypeBinding.getDimensions() == 0) {
            int constructorArguments = constructorArguments(iTypeBinding);
            String simpleName = iTypeBinding.getName();
            if (enteredText != null && enteredText.length() > 0) {
                if (!com.stmare.str.StrUtil.startsWithIgnoreCase(simpleName, enteredText)) {
                    return;
                }
            }
            String qualifiedName = iTypeBinding.getQualifiedName();
            int indexOf = qualifiedName.indexOf("<");
            if (indexOf != -1) {
                qualifiedName = qualifiedName.substring(0, indexOf);
            }
            String simpleNameStr = qualifiedName;
            int lastIndexOf = qualifiedName.lastIndexOf(".");
            if (lastIndexOf != -1) {
                simpleNameStr = simpleNameStr.substring(lastIndexOf + 1);
            }
            Offer offer = new Offer(qualifiedName, simpleNameStr, iTypeBinding.getPackage().getName(), iTypeBinding.getTypeArguments().length > 0, constructorArguments);
            offer.setSourceFileType(sourceFileType);
            typeBindings.add(offer);
        }
    }
    
    private void addMethodReturnType(IMethodBinding methodBinding, Set<Offer> typeBindings, String enteredText, MethodDeclaration methodDeclaration) {
        ITypeBinding returnType = methodBinding.getReturnType();
        addTypeBinding(typeBindings, enteredText, returnType, methodDeclaration.getReturnType2().toString());
    }
    
    public static class Offer {
        private String fullyQualifiedName;

        private String simpleTypeName;
        
        /**
         * Nazev package, nebo prazdna hodnota "" pokud se jedna o root package
         */
        private String packageName;

        private boolean generic;
        
        private int constructorArgs;
        
        /**
         * Typ tak jak je uveden ve zdrojaku. Z neho se pak kopiruje
         */
        private String sourceFileType;
        
        public Offer(String fullyQualifiedName, String simpleTypeName, String packageName, boolean generic, int constructorArgs) {
            super();
            this.fullyQualifiedName = fullyQualifiedName;
            this.simpleTypeName = simpleTypeName;
            this.packageName = packageName;
            this.generic = generic;
            this.constructorArgs = constructorArgs;
        }

        public String getFullyQualifiedName() {
            return fullyQualifiedName;
        }

        public String getSimpleTypeName() {
            return simpleTypeName;
        }

        public String getPackageName() {
            return packageName;
        }

        public boolean isGeneric() {
            return generic;
        }

        public int getConstructorArgs() {
            return constructorArgs;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + constructorArgs;
            result = prime * result + ((fullyQualifiedName == null) ? 0 : fullyQualifiedName.hashCode());
            result = prime * result + (generic ? 1231 : 1237);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Offer other = (Offer) obj;
            if (constructorArgs != other.constructorArgs)
                return false;
            if (fullyQualifiedName == null) {
                if (other.fullyQualifiedName != null)
                    return false;
            } else if (!fullyQualifiedName.equals(other.fullyQualifiedName))
                return false;
            if (generic != other.generic)
                return false;
            return true;
        }

        public String getSourceFileType() {
            return sourceFileType;
        }

        public void setSourceFileType(String sourceFileType) {
            this.sourceFileType = sourceFileType;
        }
        
    }
}
