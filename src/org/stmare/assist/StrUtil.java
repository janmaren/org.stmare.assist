package org.stmare.assist;


public class StrUtil {

    /**
     * Vraci true pokud string na danem indexu obsahuje danou sequenci zpet
     * @param str
     * @param index posledni pismeno sequence
     * @param sequence
     * @return
     */
    public static boolean isSequenceBack(String str, int index, String sequence) {
        boolean matches = true;
        for (int i = index, j = sequence.length() - 1; i > 0 && j > 0; i--, j--) {
            if (str.charAt(i) != sequence.charAt(j)) {
                matches = false;
                break;
            }
        }
        return matches;
    }
    
    /**
     * Najde posledni pozici neprazdneho znaku zpet
     * @param str
     * @param fromPos
     * @return
     */
    public static int findNonWhiteSpacePosBackward(String str, final int fromPos) {
        int index = fromPos - 1;
        while (index >= 0) {
            char charAt = str.charAt(index);
            if (Character.isWhitespace(charAt)) {
                index --;
            } else {
                return index;
            }
        }
        return -1;
    }
    
    public static int findNonIntervalPosBackward(String str, final int fromPos, CharInterval[] intervals) {
        int index = fromPos - 1;
        while (index >= 0) {
            char charAt = str.charAt(index);
            if (charInInterval(charAt, intervals)) {
                index --;
            } else {
                return index;
            }
        }
        return -1;
    }    
    
    /**
     * Vraci true pokud je znak v jednom z intervalu
     * @param ch
     * @param intervals
     * @return
     */
    public static boolean charInInterval(char ch, CharInterval[] intervals) {
        for (CharInterval charInterval : intervals) {
            if (ch >= charInterval.from && ch <= charInterval.to) {
                return true;
            }
        }
        return false;
    }
    
    public static class CharInterval {
        public char from;
        
        /**
         * Including this
         */
        public char to;

        public CharInterval(char from, char to) {
            super();
            this.from = from;
            this.to = to;
        }
    }
}
